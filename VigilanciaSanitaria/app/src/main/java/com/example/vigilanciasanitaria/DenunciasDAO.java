package com.example.vigilanciasanitaria;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

import com.example.vigilanciasanitaria.Denuncias;

public class DenunciasDAO {

    private Conexao conn;
    private String TABLE = "DENUNCIAS";

    public DenunciasDAO(Context context) {
        conn = new Conexao(context);
    }

    public void salvar(Denuncias denuncias) {
        SQLiteDatabase db = conn.getWritableDatabase();
        ContentValues dados = inserirDados(denuncias);

        db.insert(TABLE, null, dados);
        db.close();
    }


    public List<Denuncias> listarDenuncias() {
        List<Denuncias> list = new ArrayList<>();

        String sql = "SELECT * FROM DENUNCIAS;";
        SQLiteDatabase db = conn.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()){
            int id = cursor.getInt(cursor.getColumnIndex("ID"));
            String nome_do_estabelecimento = cursor.getString(cursor.getColumnIndex("NOME_DO_ESTABELECIMENTO"));
            String descricao = cursor.getString(cursor.getColumnIndex("DESCRICAO"));
            String rua = cursor.getString(cursor.getColumnIndex("RUA"));
            String cidade = cursor.getString(cursor.getColumnIndex("CIDADE"));
            String uf = cursor.getString(cursor.getColumnIndex("UF"));
            String cep = cursor.getString(cursor.getColumnIndex("CEP"));
            String numero = cursor.getString(cursor.getColumnIndex("NUMERO"));
            String foto = cursor.getString(cursor.getColumnIndex("FOTO"));

            list.add(new Denuncias(id,nome_do_estabelecimento,descricao,rua,cidade,uf,cep,numero,foto));
        }

        cursor.close();
        db.close();

        return list;
    }

    public void remover(Denuncias denuncias) {
        SQLiteDatabase db = conn.getWritableDatabase();
        String[] param = {String.valueOf(denuncias.getId())};
        db.delete(TABLE,"id = ?",param);
        db.close();
    }

    public void atualizar(Denuncias denuncias) {
        SQLiteDatabase db = conn.getWritableDatabase();
        ContentValues dados = inserirDados(denuncias);
        String[] param = {String.valueOf(denuncias.getId())};

        db.update(TABLE,dados,"id = ?",param);
        db.close();
    }


    private ContentValues inserirDados(Denuncias denuncias) {

        ContentValues dados = new ContentValues();
        dados.put("NOME_DO_ESTABELECIMENTO", denuncias.getNome_do_estabelecimento());
        dados.put("DESCRICAO", denuncias.getDescricao());
        dados.put("RUA", denuncias.getRua());
        dados.put("CIDADE", denuncias.getCidade());
        dados.put("UF", denuncias.getUf());
        dados.put("CEP", denuncias.getCep());
        dados.put("NUMERO", denuncias.getNumero());
        dados.put("FOTO", denuncias.getFoto());
        return dados;
    }

}