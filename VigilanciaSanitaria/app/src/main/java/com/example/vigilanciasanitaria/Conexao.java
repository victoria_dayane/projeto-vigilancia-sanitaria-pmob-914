package com.example.vigilanciasanitaria;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Conexao extends SQLiteOpenHelper {

    public Conexao(Context context) {
        super(context, "LISTADENUNCIAS", null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE DENUNCIAS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NOME_DO_ESTABELECIMENTO TEXT NOT NULL, DESCRICAO TEXT," +
                "RUA TEXT, CIDADE TEXT, UF TEXT, CEP TEXT, NUMERO TEXT, FOTO TEXT);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS DENUNCIAS";
        db.execSQL(sql);

        onCreate(db);
    }

}