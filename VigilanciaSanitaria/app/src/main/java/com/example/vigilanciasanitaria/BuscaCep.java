package com.example.vigilanciasanitaria;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class BuscaCep extends AsyncTask<String, Object, String> {

    private WeakReference<ActivityFormulario> reference;

    public BuscaCep(Context context) {
        this.reference = new WeakReference<>(
                (ActivityFormulario) context);
    }

    @Override
    protected String doInBackground(String... listaCep) {
        String cep = listaCep[0];

        WebCliente client = new WebCliente();
        String response = client.getCep(cep);

        System.out.println(response);

        return response;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        try {
            JSONObject json = new JSONObject(s);
            String rua = json.getString("rua");
            String cidade = json.getString("cidade");
            String uf = json.getString("uf");

            EditText editTextRua = reference.get().findViewById(R.id.edittext_rua);
            EditText editTextCidade = reference.get().findViewById(R.id.edittext_cidade);
            EditText editTextUF = reference.get().findViewById(R.id.edittext_uf);

            editTextRua.setText(rua);
            editTextCidade.setText(cidade);
            editTextUF.setText(uf);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
