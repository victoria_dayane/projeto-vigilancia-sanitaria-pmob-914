package com.example.vigilanciasanitaria;

import android.Manifest;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import java.util.List;

import com.example.vigilanciasanitaria.R;
import com.example.vigilanciasanitaria.DenunciasDAO;
import com.example.vigilanciasanitaria.Denuncias;

public class ListaDenunciasActivity extends AppCompatActivity {
	
	private AdapterDenuncias adapter;
    private Denuncias denuncias;
    private DenunciasDAO dao = new DenunciasDAO(this);
	
	
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
		
		adapter = new AdapterDenuncias(dao.listarDenuncias(), this);
		
		ListView listView = findViewById(R.id.listview_mainactivity);
        listView.setAdapter(adapter);
		
		registerForContextMenu(listView);
    }
	
	@Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
    }
	
	@Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo contextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        denuncias = (Denuncias) adapter.getItem(contextMenuInfo.position);

        
        if (item.getItemId() == R.id.menu_item_editar) {
            Intent i = new Intent(ListaDenunciasActivity.this, ActivityFormulario.class);
            i.putExtra("denuncias", denuncias);
            startActivity(i);
        }

        if (item.getItemId() == R.id.menu_item_excluir) {
            DenunciasDAO dao = new DenunciasDAO(this);
            dao.remover(denuncias);
            onResume();
        }
		
		if (item.getItemId() == R.id.menu_end_denuncia) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("geo:0,0?q=" + denuncias.getRua()));
            startActivity(i);
        }
		
		return super.onContextItemSelected(item);
    }
	
	@SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}
	
	@Override
    protected void onResume() {
        super.onResume();
        adapter.atualizar(dao.listarDenuncias());
    }
}