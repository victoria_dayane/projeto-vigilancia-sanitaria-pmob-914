package com.example.vigilanciasanitaria;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.Manifest;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import java.io.File;

public class ActivityFormulario extends AppCompatActivity {
    private String caminhoFoto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formulario);

        Button btnCep = findViewById(R.id.btn_cep);
        btnCep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextcep = findViewById(R.id.edittext_cep);
                String cep = editTextcep.getText().toString();

                BuscaCep task = new BuscaCep(ActivityFormulario.this);
                task.execute(cep);
            }
        });

        Intent intent = getIntent();
        if(intent.hasExtra("denuncias")){
            EditText editTextNomeEstabelecimento = findViewById(R.id.edittext_nome_estabelecimento);
            EditText editTextDescricao = findViewById(R.id.edittext_descricao);
            EditText editTextRua = findViewById(R.id.edittext_rua);
            EditText editTextCidade = findViewById(R.id.edittext_cidade);
            EditText editTextUF = findViewById(R.id.edittext_uf);
            EditText editTextCep = findViewById(R.id.edittext_cep);
            EditText editTextNumero = findViewById(R.id.edittext_numero);


            Denuncias denuncias = (Denuncias) intent.getSerializableExtra("denuncias");
            editTextNomeEstabelecimento.setText(denuncias.getNome_do_estabelecimento());
            editTextDescricao.setText(denuncias.getDescricao());
            editTextRua.setText(denuncias.getRua());
            editTextCidade.setText(denuncias.getCidade());
            editTextUF.setText(denuncias.getUf());
            editTextCep.setText(denuncias.getCep());
            editTextNumero.setText(denuncias.getNumero());
            setImage(denuncias.getFoto());
        }

        Button btnfoto = findViewById(R.id.btn_img);
        btnfoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                caminhoFoto = getExternalFilesDir(null) + "/" + System.currentTimeMillis() + ".jpg";
                File file = new File(caminhoFoto);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        FileProvider.getUriForFile(
                                ActivityFormulario.this,
                                BuildConfig.APPLICATION_ID + ".provider",
                                file
                        ));

                startActivityForResult(intent, 123);

            }
        });

        Button button = findViewById(R.id.button_form);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextNomeEstabelecimento = findViewById(R.id.edittext_nome_estabelecimento);
                EditText editTextDescricao = findViewById(R.id.edittext_descricao);
                EditText editTextRua = findViewById(R.id.edittext_rua);
                EditText editTextCidade = findViewById(R.id.edittext_cidade);
                EditText editTextUF = findViewById(R.id.edittext_uf);
                EditText editTextCep = findViewById(R.id.edittext_cep);
                EditText editTextNumero = findViewById(R.id.edittext_numero);
                ImageView imageFoto = findViewById(R.id.form_img);

                String nomeestabelecimento = editTextNomeEstabelecimento.getText().toString();
                String descricao = editTextDescricao.getText().toString();
                String rua = editTextRua.getText().toString();
                String cidade = editTextCidade.getText().toString();
                String uf = editTextUF.getText().toString();
                String cep = editTextCep.getText().toString();
                String numero = editTextNumero.getText().toString();
                String foto = imageFoto.getTag().toString();

                DenunciasDAO dao = new DenunciasDAO(ActivityFormulario.this);

                Intent intent = getIntent();
                if(intent.hasExtra("denuncias")) {
                    Denuncias denuncias = (Denuncias) intent.getSerializableExtra("denuncias");
                    denuncias.setNome_do_estabelecimento(nomeestabelecimento);
                    denuncias.setDescricao(descricao);
                    denuncias.setRua(rua);
                    denuncias.setCidade(cidade);
                    denuncias.setUf(uf);
                    denuncias.setCep(cep);
                    denuncias.setNumero(numero);
                    denuncias.setFoto(foto);

                    dao.atualizar(denuncias);
                    System.out.println(denuncias.getId()  + " " + denuncias.toString());

                } else {
                    Denuncias denuncias = new Denuncias(nomeestabelecimento,descricao,rua,cidade,uf,cep,numero);
                    dao.salvar(denuncias);
                }

                finish();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setImage(caminhoFoto);
    }

    private void setImage(String foto) {
        ImageView imageView = findViewById(R.id.form_img);
        Bitmap bitmap = BitmapFactory.decodeFile(foto);
        imageView.setImageBitmap(bitmap);
        imageView.setTag(foto);
    }

}

