package com.example.vigilanciasanitaria;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle a) {
        super.onCreate(a);
        setContentView(R.layout.tela_inicial);
		
		ActionBar actionbar = getSupportActionBar();
        if(actionbar != null){
            actionbar.setBackgroundDrawable(getResources().getDrawable(R.drawable.mygradient));
        }

        ImageButton bt_fazer_denuncia = findViewById(R.id.id_botao_fazer_denuncia);
        bt_fazer_denuncia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        ActivityFormulario.class);
                startActivity(intent);
            }
        });
        ImageButton bt_ver_denuncia = findViewById(R.id.id_botao_ver_denuncia);
        bt_ver_denuncia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        ListaDenunciasActivity.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.activity_sobre){
            Intent i = new Intent(MainActivity.this, ActivitySobre.class);
            startActivity(i);

        }

        else if(item.getItemId() == R.id.activity_lista_menu_ligar){

            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                Intent i = new Intent(Intent.ACTION_CALL);
                i.setData(Uri.parse("tel:3530-3302"));
                startActivity(i);
            }
        } else if(item.getItemId() == R.id.activity_lista_menu_end){
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("geo:0,0?q=R. Samaritana, 1185 - Santa Edwiges, Arapiraca - AL, 57311-185"));
            startActivity(i);
        }

       return super.onOptionsItemSelected(item);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (permissions[0].equals(Manifest.permission.CALL_PHONE) &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Intent i = new Intent(Intent.ACTION_CALL);
            i.setData(Uri.parse("tel:3530-3302"));
            startActivity(i);
        }
    }


        // @Override
    //protected void onResume() {
       // super.onResume();

        //LIMPAR A LISTA E ADCIONAR OS OBJETOS NOVAMENTE
       // adapter.clear();
        //adapter.addAll(lista);
   // }



}
