package com.example.vigilanciasanitaria;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class AdapterDenuncias extends BaseAdapter {
    private List<Denuncias> listaDenuncias;
    private Context context;

    public AdapterDenuncias(List<Denuncias> listaDenuncias, Context context) {
        this.listaDenuncias = listaDenuncias;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listaDenuncias.size();
    }

    @Override
    public Denuncias getItem(int position) {
        return listaDenuncias.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listaDenuncias.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.activity_list, parent, false);

        Denuncias d = listaDenuncias.get(position);

        TextView textViewNome = view.findViewById(R.id.edittext_nome_estabelecimento);
        TextView textViewDescricao = view.findViewById(R.id.edittext_descricao);
        TextView textViewRua = view.findViewById(R.id.edittext_rua);
        TextView textViewCidade = view.findViewById(R.id.edittext_cidade);
        TextView textViewUf = view.findViewById(R.id.edittext_uf);
        TextView textViewCep = view.findViewById(R.id.edittext_cep);
        TextView textViewNumero = view.findViewById(R.id.edittext_numero);

        ImageView imagem = (ImageView) view.findViewById(R.id.form_img);

        String foto = d.getFoto();

        Bitmap bitmap = BitmapFactory.decodeFile(foto);
        imagem.setImageBitmap(bitmap);
        imagem.setTag(foto);

        textViewNome.setText(d.getNome_do_estabelecimento());
        textViewCep.setText(d.getCep());

        if(textViewCep != null){
            String cep = d.getRua() + ", " + d.getCidade() + " - " + d.getUf();
            textViewCep.setText(cep);
            textViewCep.setText(d.getCep());
        }

        return view;
    }

    public void atualizar(List<Denuncias> denuncias) {
        listaDenuncias.clear();
        listaDenuncias.addAll(denuncias);
        notifyDataSetChanged();
    }

}
