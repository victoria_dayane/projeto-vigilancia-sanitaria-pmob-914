package com.example.vigilanciasanitaria;

import java.io.Serializable;

public class Denuncias implements Serializable {
    private int id;
    private String nome_do_estabelecimento, descricao, cep, rua, cidade, uf, numero, foto;

    public Denuncias(String nome_do_estabelecimento, String descricao, String cep, String rua, String cidade, String uf, String numero) {
        this.id = 0;
        this.nome_do_estabelecimento = nome_do_estabelecimento;
        this.descricao = descricao;
        this.rua = rua;
        this.cidade = cidade;
        this.uf = uf;
        this.cep = cep;
        this.numero = numero;
    }

    public Denuncias(int id, String nome_do_estabelecimento, String descricao, String rua, String cidade, String uf, String cep, String numero) {
        this.id = id;
        this.nome_do_estabelecimento = nome_do_estabelecimento;
        this.descricao = descricao;
        this.rua = rua;
        this.cidade = cidade;
        this.uf = uf;
        this.cep = cep;
        this.numero = numero;
    }
    public Denuncias(int id, String nome_do_estabelecimento, String descricao, String rua, String cidade, String uf, String cep, String numero, String foto) {
        this.id = id;
        this.nome_do_estabelecimento = nome_do_estabelecimento;
        this.descricao = descricao;
        this.rua = rua;
        this.cidade = cidade;
        this.uf = uf;
        this.cep = cep;
        this.numero = numero;
        this.foto = foto;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getNome_do_estabelecimento() {
        return nome_do_estabelecimento;
    }

    public void setNome_do_estabelecimento(String nome_do_estabelecimento) {
        this.nome_do_estabelecimento = nome_do_estabelecimento;
    }

    public String getDescricao() { return descricao; }

    public void setDescricao(String descricao) { this.descricao = descricao; }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getFoto() { return foto;}

    public void setFoto(String foto) {
        this.foto = foto;
    }


    @Override
    public String toString() {
        return "Denuncias{" +
                "id=" + id +
                ", nome_do_estabelecimento='" + nome_do_estabelecimento + '\'' +
                ", descricao='" + descricao + '\'' +
                ", cep='" + cep + '\'' +
                ", rua='" + rua + '\'' +
                ", cidade='" + cidade + '\'' +
                ", uf='" + uf + '\'' +
                ", numero='" + numero + '\'' +
                ", foto='" + foto + '\'' +
                '}';
    }
}